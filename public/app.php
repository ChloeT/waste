<?php


use App\Service\Decoder;
use App\Service\ValuesProvider;
use App\UseCase\SortingCenter;

require __DIR__ . '/../vendor/autoload.php';

$sortingCenter = new SortingCenter("data.json", __DIR__."/log");
$sortingCenter->sort();