<?php

namespace App\UseCase;

use App\Service\Decoder;
use App\Entity\Waste;
use App\Entity\Center;
use App\Service\ValuesProvider;

class SortingCenter
{
    private Decoder $decoder;
    private TreatmentHandler $treatment;
    private Center $incinerator;
    private ValuesProvider $provider;

    public function __construct(string $fileToDecode, string $logFilePath)
    {
        $this->decoder = new Decoder($fileToDecode);
        $this->treatment = new TreatmentHandler($logFilePath);
        $this->setIncinerator();
        $this->provider = new ValuesProvider;
    }

    public function sort()
    {
        $this->sortGrey();
        $this->sortPaper();
        $this->sortGreens();
        $this->sortGlass();
        $this->sortMetal();

        $this->sortPlastics();

        print("done \n");
    }




    private function sortGrey()
    {
        $volume = array_sum($this->decoder->getWastes("grey"));
        $waste = new Waste($volume, "grey");

        $this->treatment->treat($waste, $this->incinerator);
    }



    private function sortPaper()
    {
        $volume = array_sum($this->decoder->getWastes("paper"));
        $waste = new Waste($volume, "paper");

        $capacity = array_sum($this->decoder->getPaperTraitements());
        $center = new Center($capacity, ["paper"], "retreat");

        $this->treatment->treat($waste, $center);

        if ($waste->getVolume() != 0) {
            $this->treatment->treat($waste, $this->incinerator);
        }
    }



    private function sortGreens()
    {
        $volume = array_sum($this->decoder->getWastes("green"));
        $waste = new Waste($volume, "green");

        $capacity = array_sum($this->decoder->getComposts());
        $center = new Center($capacity, ["green"], "retreat");

        $this->treatment->treat($waste, $center);

        if ($waste->getVolume() != 0) {
            $this->treatment->treat($waste, $this->incinerator);
        }
    }



    private function sortGlass()
    {
        $volume = array_sum($this->decoder->getWastes("glass"));
        $waste = new Waste($volume, "glass");

        $capacity = array_sum($this->decoder->getGlassTraitements());
        $center = new Center($capacity, ["glass"], "retreat");

        $this->treatment->treat($waste, $center);

        if ($waste->getVolume() != 0) {
            $this->treatment->treat($waste, $this->incinerator);
        }
    }



    private function sortMetal()
    {
        $volume = array_sum($this->decoder->getWastes("metal"));
        $waste = new Waste($volume, "metal");

        $capacity = array_sum($this->decoder->getMetalTraitements());
        $center = new Center($capacity, ["metal"], "retreat");

        $this->treatment->treat($waste, $center);

        if ($waste->getVolume() != 0) {
            $this->treatment->treat($waste, $this->incinerator);
        }
    }





    private function sortPlastics()
    {
        // recupère les dechets plastiques dans un tableau
        $plasticWastes = [];
        // permet de recycler en 1er le type de plastique le + poluant
        $plasticTypes = $this->provider->plasticsPriority();

        foreach ($plasticTypes as $plastic) {
            $volume = array_sum($this->decoder->getWastes($plastic));
            $plasticWastes[] = new Waste($volume, $plastic);
        }

        // recupère les centres de traitements des plastiques dans un tableau 
        $centers = [];
        $centersDatas = $this->decoder->getPlasticsTraitements();

        foreach ($centersDatas as $datas) {
            $centers[] = new Center($datas["capacity"], $datas["plasticTypes"], "retreat");
        }

        // traitement dans les diferents centers en fonction de leurs capacités et de types de plastique acceptés
        foreach ($plasticWastes as $waste) {
            foreach ($centers as $center) {
                $this->treatment->treat($waste, $center);

                if ($waste->getVolume() == 0) {
                    break;
                }
            }
            if ($waste->getVolume() > 0) {
                $this->treatment->treat($waste, $this->incinerator);
            }
        }
    }





    private function setIncinerator()
    {
        $incineratorsCapacity = array_sum($this->decoder->getIncinerators());
        $this->incinerator = new Center($incineratorsCapacity, ["grey", "PET", "PVC", "PC", "PEHD", "paper", "green", "metal", "glass"], "burn");
    }
}
