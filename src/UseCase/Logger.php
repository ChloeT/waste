<?php

namespace App\UseCase;

use App\Service\LoggerInterface;

class Logger implements LoggerInterface
{

    private string $filePath;

    public function __construct(string $filePath)
    {
        $this->createFile($filePath);
    }


    public function createLog(string $log): void
    {
        file_put_contents($this->filePath, " " . $log . "\n", FILE_APPEND);
    }

    public function createFile($filePath): void
    {
        $parts = explode('/', $filePath);
        array_pop($parts);
        $createdPath = '';

        foreach ($parts as $part) {
            if (!is_dir($createdPath .= "/$part")) {
                mkdir($part);
            }
        }

        $this->filePath = $filePath . date("_d-m-Y_h:i:s");

        file_put_contents($this->filePath, "Logs on " . date("d-m-Y_h:i:s") . "\n", FILE_APPEND);

    }
}
