<?php

namespace App\UseCase;

use App\Service\centerInterface;
use App\Service\ValuesProvider;
use App\Service\WasteInterface;
use App\UseCase\Logger;

class TreatmentHandler
{

    private ValuesProvider $valuesProvider;
    private Logger $logger;


    public function __construct(string $filePath)
    {
        $this->valuesProvider = new ValuesProvider;
        $this->logger = new Logger($filePath);
    }



    public function treat(WasteInterface $waste, centerInterface $center)
    {
        if (!$this->isAccepted($waste->getType(), $center->getAcceptedTypes())) {
            return;
        }

        $treatedVolume = 0;
        if ($center->getcapacity() > $waste->getVolume()) {
            $treatedVolume = $waste->getVolume();
        } else {
            $treatedVolume = $center->getcapacity();
        }

        $center->treat($treatedVolume);
        $waste->beTreated($treatedVolume);

        $this->calcCarbonCost($waste->getType(), $center->getTreatmentMethod(), $treatedVolume);

        $this->logger->createLog("remaining capacity for this center : " . $center->getcapacity() . "\n remaining volume of " . $waste->getType() . " to treat : " . $waste->getVolume() . "\n");
    }


    private function calcCarbonCost(string $type, string $method, float $treatedVolume)
    {
        $unitCarbonCost = $this->valuesProvider->carbonCost($type, $method);
        $totalCarbonCost = $treatedVolume * $unitCarbonCost;
        $this->logger->createLog($treatedVolume . " of " . $type . " " . $method . "ed, " . $totalCarbonCost . " of carbon rejected");
        return $totalCarbonCost;
    }



    private function isAccepted(string $type, array $acceptedTypes)
    {
        if (in_array($type, $acceptedTypes)) {

            return true;
        } else {

            $this->logger->createLog("this center doesn't accept " . $type);

            return false;
        }
    }
}
