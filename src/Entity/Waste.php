<?php

namespace App\Entity;

use App\Service\WasteInterface;

class Waste implements WasteInterface
{
    private string $type;
    private float $volume;

    public function __construct(float $volume, string $type)
    {
        $this->type = $type;
        $this->volume = $volume;
    }


    public function beTreated(float $volume): void
    {
        $this->volume -= $volume;

    }


    public function getVolume(): float
    {
        return $this->volume;
    }


    public function getType(): string
    {
        return $this->type;
    }
}
