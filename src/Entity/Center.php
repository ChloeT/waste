<?php

namespace App\Entity;

use App\Service\CenterInterface;
use App\Service\ValuesProvider;

class Center implements CenterInterface
{
    private float $capacity;
    private array $acceptedTypes;
    private string $treatmentMethod;


    public function __construct(float $capacity, array $acceptedTypes, string $treatmentMethod)
    {
        $this->capacity = $capacity;
        $this->treatmentMethod = $treatmentMethod;
        $this->acceptedTypes = $acceptedTypes;
    }


    public function treat(float $volume): float
    {
        $treatedVolume = $volume;
        if ($this->capacity - $volume < 0) {
            $treatedVolume = $this->capacity;
        }
        $this->capacity -= $treatedVolume;
        return $treatedVolume;
    }


    public function getAcceptedTypes(): array
    {
        return $this->acceptedTypes;
    }


    public function getTreatmentMethod(): string
    {
        return $this->treatmentMethod;
    }


    public function getcapacity(): float
    {
        return $this->capacity;
    }
}
