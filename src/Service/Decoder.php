<?php

namespace App\Service;

class Decoder
{
    private $quartiers;
    private $services;
    private ValuesProvider $valueProvider;

    public function __construct(string $filePath)
    {
        $data = json_decode(file_get_contents($filePath), true);
        $this->quartiers = $data["quartiers"];
        $this->services = $data["services"];
        $this->valueProvider = new ValuesProvider;
    }


    public function getWastes($type): array
    {
        $crap = [];
        $plasticTypes = null;

        foreach ($this->quartiers as $wastes) {

            if ($type == "paper") {
                $crap[] = $wastes["papier"];
            } elseif ($type == "green") {
                $crap[] = $wastes["organique"];
            } elseif ($type == "glass") {
                $crap[] = $wastes["verre"];
            } elseif ($type == "metal") {
                $crap[] = $wastes["metaux"];
            } elseif ($type == "grey") {
                $crap[] = $wastes["autre"];
            } elseif ($type == "PET" || $type == "PVC" || $type == "PC" || $type == "PEHD") {
                $plasticTypes[] = $wastes["plastiques"];
            }
        }

        if ($plasticTypes) {
            foreach ($plasticTypes as $plastics) {
                $crap[] = $plastics[$type];
            }
        }
        return $crap;
    }


    // treatments

    public function getIncinerators()
    {
        $incinerators = [];

        foreach ($this->services as $service) {
            if ($service["type"] == "incinerateur") {
                $LineCapacity = $service["capaciteLigne"];
                $lines = $service["ligneFour"];
                $incinerators[] = $lines * $LineCapacity;
            }
        }
        return $incinerators;
    }


    public function getNonPlasticTraitements($type): array
    {     
        if ($type == "paper") {
            return $this->getPaperTraitements();
        } elseif ($type == "green") {
            return $this->getComposts();
        } elseif ($type == "glass") {
            return $this->getGlassTraitements();
        } elseif ($type == "metal") {
            return $this->getMetalTraitements();
        }
    }


    public function getPlasticsTraitements()
    {
        $plasticTraitements = [];

        foreach ($this->services as $service) {
            if ($service["type"] == "recyclagePlastique") {
                $traitement = [
                    'capacity' => $service["capacite"],
                    'plasticTypes' => $service["plastiques"]
                ];
                $plasticTraitements[] = $traitement;
            }
        }
        return $plasticTraitements;
    }



    public function getPaperTraitements()
    {
        $paperRecycle = [];

        foreach ($this->services as $service) {
            if ($service["type"] == "recyclagePapier") {
                $paperRecycle[] = $service["capacite"];
            }
        }

        return $paperRecycle;
    }


    public function getGlassTraitements()
    {
        $glassRecycle = [];

        foreach ($this->services as $service) {
            if ($service["type"] == "recyclageVerre") {
                $glassRecycle[] = $service["capacite"];
            }
        }

        return $glassRecycle;
    }


    public function getMetalTraitements()
    {
        $metalRecycle = [];

        foreach ($this->services as $service) {
            if ($service["type"] == "recyclageMetaux") {
                $metalRecycle[] = $service["capacite"];
            }
        }

        return $metalRecycle;
    }


    public function getComposts()
    {
        $compost = [];

        foreach ($this->services as $service) {
            if ($service["type"] == "composteur") {
                $compost[] = $service["capacite"];
            }
        }

        return $compost;
    }




    /**
     * just print the returns of all the methods to check if everything works fine
     */
    public function tester()
    {
        print_r("wastes");

        print_r("papers");
        print_r($this->getWastes("paper"));

        print_r("greens");
        print_r($this->getWastes("green"));

        print_r("glasses");
        print_r($this->getWastes("glass"));

        print_r("metals");
        print_r($this->getWastes("metal"));

        print_r("greys");
        print_r($this->getWastes("grey"));

        print_r("PET");
        print_r($this->getWastes("PET"));

        print_r("PVC");
        print_r($this->getWastes("PVC"));

        print_r("PC");
        print_r($this->getWastes("PC"));

        print_r("PEHD");
        print_r($this->getWastes("PEHD"));


        print_r("services");


        print_r("incinerator services");
        print_r($this->getIncinerators());

        print_r("plastics services");
        print_r($this->getPlasticsTraitements());

        print_r("paper services");
        print_r($this->getPaperTraitements());

        print_r("glass services");
        print_r($this->getGlassTraitements());

        print_r("metaux services");
        print_r($this->getMetalTraitements());

        print_r("compost services");
        print_r($this->getComposts());
    }
}
