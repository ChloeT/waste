<?php

namespace App\Service;

interface LoggerInterface{
    public function createLog(string $log):void;
}