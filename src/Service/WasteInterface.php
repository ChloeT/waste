<?php

namespace App\Service;

interface WasteInterface {

    public function beTreated(float $volume):void;
    public function getVolume():float;
    public function getType():string;
}