<?php

namespace App\Service;

class ValuesProvider
{
    private array $values =
    [
        "grey"  =>  [
            "burn" => 30
        ],
        "PVC"   =>  [
            "burn" => 38,
            "retreat" => 12
        ],
        "PC"    =>  [
            "burn" => 42,
            "retreat" => 10
        ],
        "PET"   =>  [
            "burn" => 40,
            "retreat" => 8
        ],
        "PEHD"  =>  [
            "burn" => 35,
            "retreat" => 11
        ],
        "paper" =>  [
            "burn" => 25,
            "retreat" => 5
        ],
        "green" =>  [
            "burn" => 28,
            "retreat" => 1
        ],
        "metal" =>  [
            "burn" => 50,
            "retreat" => 7
        ],
        "glass" =>  [
            "burn" => 50,
            "retreat" => 6
        ],
    ];

    private array $plastics = ["PEHD", "PET", "PC", "PVC"];


    public function checkType(string $type)
    {
        return in_array($type, array_keys($this->values));
    }


    public function carbonCost(string $type, string $method)
    {
        if ($this->checkType($type) & in_array($method, array_keys($this->values[$type]))) {
            return $this->values[$type][$method];
        }
    }


    public function plasticsPriority()
    {

        $types = [];
        foreach (array_keys($this->values) as $value) {
            if (in_array($value, $this->plastics)) {
                $types[] = $value;
            }
        }

        $methodDiferences = [];

        foreach ($types as $type) {
            $methodDiference = $this->values[$type]["burn"] - $this->values[$type]["retreat"];
            $methodDiferences[$type] = $methodDiference;
        }


        arsort($methodDiferences);

        return array_keys($methodDiferences);
    }
}
