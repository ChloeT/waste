<?php

namespace App\Service;

interface CenterInterface
{
    public function treat(float $volume): float;
    public function getAcceptedTypes(): array;
    public function getTreatmentMethod(): string;
    public function getcapacity(): float;
}
